#include <iostream>

class Fahrenheit;

class Celsius
{
public:
	double m_value;
	Celsius(double val);
	Celsius(Fahrenheit val);
	Celsius Convert(Fahrenheit fahrenheit);
	Fahrenheit Convert();
	friend std::ostream& operator<<(std::ostream& os, const Celsius& celsius);
};

class Fahrenheit
{
public:
	double m_value;
	Fahrenheit(double val);
	Fahrenheit(Celsius val);
	Fahrenheit Convert(Celsius celsius);
	Celsius Convert();
	friend std::ostream& operator<<(std::ostream& os, const Fahrenheit& fahrenheit);
};

Fahrenheit::Fahrenheit(double val)
{
	m_value = val;
}

Fahrenheit::Fahrenheit(Celsius val)
{
	m_value = (val.m_value * 1.8) + 32;
}

Fahrenheit Fahrenheit::Convert(Celsius celsius)
{
	return Fahrenheit((celsius.m_value * 1.8) + 32);
}

Celsius Fahrenheit::Convert()
{
	return Celsius(*this);
}

Celsius::Celsius(double val)
{
	m_value = val;
}

Celsius::Celsius(Fahrenheit val)
{
	m_value = (val.m_value - 32) / 1.8;
}

Celsius Celsius::Convert(Fahrenheit fahrenheit)
{
	return Celsius((fahrenheit.m_value - 32) / 1.8);
}

Fahrenheit Celsius::Convert()
{
	return Fahrenheit(*this);
}

std::ostream& operator<<(std::ostream& os, const Fahrenheit& fahrenheit)
{
	os << "fahrenheit: " << fahrenheit.m_value << " degrees" << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream& os, const Celsius& celsius)
{
	os << "celsius: " << celsius.m_value << " degrees" << std::endl;
	return os;
}