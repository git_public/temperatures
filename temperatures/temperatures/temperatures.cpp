#include "temperatures.h"

int main()
{
	Celsius wb_c(100);
	Celsius wf_c(0);
	Celsius oz_c(-273.15);
	Fahrenheit a(wb_c), b(wf_c), c(oz_c);
	std::cout << wb_c << a << wf_c << b << oz_c << c;
	std::cout << wb_c << wb_c.Convert() << wf_c << wf_c.Convert() << oz_c << oz_c.Convert();
	return 0;
}